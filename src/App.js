import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    newTextDescription: "",
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  addTodo = (e) => {
    if (e.keyCode === 13) {
      const newTodos = this.state.todos;
      const newItem = {
        userId: 1,
        id: this.state.todos.length + 1,
        title: this.state.newTextDescription,
        completed: false,
      };
      newTodos.push(newItem);
      this.setState({ todos: newTodos, newTextDescription: "" });
    }
  };

  toggleComplete = (id) => (e) => {
    const newTodos = this.state.todos;

    newTodos.forEach((todo) => {
      if (todo.id === id) {
        todo.completed = !todo.completed;
      }
    });
    this.setState({ todos: newTodos });
  };

  handleDelete = (id) => (e) => {
    const newTodos = this.state.todos.filter((todo) => todo.id !== id);

    this.setState({ todos: newTodos });
  };

  clearCompleted = () => {
    const newTodos = this.state.todos.filter(
      (todo) => todo.completed === false
    );

    this.setState({ todos: newTodos });
  };

  itemsRemaining = () => {
    const remainingTodos = this.state.todos.filter(
      (todo) => todo.completed === false
    );
    return remainingTodos.length;
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            name="newTextDescription"
            value={this.state.newTextDescription}
            onChange={this.handleChange}
            onKeyDown={this.addTodo}
            placeholder="What needs to be done?"
            autoFocus
          />
        </header>
        <TodoList
          todos={this.state.todos}
          toggleComplete={this.toggleComplete}
          handleDelete={this.handleDelete}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>{this.itemsRemaining()}</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.clearCompleted}>
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={this.props.toggleComplete}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.handleDelete} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              toggleComplete={this.props.toggleComplete(todo.id)}
              handleDelete={this.props.handleDelete(todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
